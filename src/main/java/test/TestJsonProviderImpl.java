package test;

import javax.json.*;
import javax.json.spi.JsonProvider;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;

public class TestJsonProviderImpl extends JsonProvider {

    @Override public JsonParser createParser(Reader reader) {
        return null;
    }

    @Override public JsonParser createParser(InputStream in) {
        return null;
    }

    @Override public JsonParserFactory createParserFactory(Map<String, ?> config) {
        return null;
    }

    @Override public JsonGenerator createGenerator(Writer writer) {
        return null;
    }

    @Override public JsonGenerator createGenerator(OutputStream out) {
        return null;
    }

    @Override public JsonGeneratorFactory createGeneratorFactory(Map<String, ?> config) {
        return null;
    }

    @Override public JsonReader createReader(Reader reader) {
        return null;
    }

    @Override public JsonReader createReader(InputStream in) {
        return null;
    }

    @Override public JsonWriter createWriter(Writer writer) {
        return null;
    }

    @Override public JsonWriter createWriter(OutputStream out) {
        return null;
    }

    @Override public JsonWriterFactory createWriterFactory(Map<String, ?> config) {
        return null;
    }

    @Override public JsonReaderFactory createReaderFactory(Map<String, ?> config) {
        return null;
    }

    @Override public JsonObjectBuilder createObjectBuilder() {
        return new TestJsonObjectBuilderImpl();
    }

    @Override public JsonArrayBuilder createArrayBuilder() {
        return null;
    }

    @Override public JsonBuilderFactory createBuilderFactory(Map<String, ?> config) {
        return null;
    }

    private static final class TestJsonObjectBuilderImpl implements JsonObjectBuilder {

        @Override public JsonObjectBuilder add(String name, JsonValue value) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, String value) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, BigInteger value) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, BigDecimal value) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, int value) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, long value) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, double value) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, boolean value) {
            return null;
        }

        @Override public JsonObjectBuilder addNull(String name) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, JsonObjectBuilder builder) {
            return null;
        }

        @Override public JsonObjectBuilder add(String name, JsonArrayBuilder builder) {
            return null;
        }

        @Override public JsonObject build() {
            return null;
        }
    }

}
