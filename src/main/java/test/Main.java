package test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public final class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        JsonObjectBuilder builder = Json.createObjectBuilder();
        logger.info("{}", builder.getClass().getCanonicalName());

        JsonObject object = builder.add("hoge", 123)
                                   .add("piyo", 456)
                                   .add("fuga", Json.createArrayBuilder().add(7).add(8).add(9))
                                   .build();

        logger.info("{}", object);

    }

}
